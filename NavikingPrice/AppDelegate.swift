//
//  AppDelegate.swift
//  NavikingPrice
//
//  Created by Wayne Lin on 2018/2/1.
//  Copyright © 2018年 Wayne Lin. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet var rootController: NSPopover!

    private var statusItem: NSStatusItem!

    private var popoverMonitor: NSEvent?

    var logManager = LogManager()

    func applicationDidFinishLaunching(_ aNotification: Notification) {

        statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)

        statusItem.button?.title = "NKP"
        statusItem.button?.target = self
        statusItem.button?.action = #selector(statusItemClick(_:))

        let controller = ViewController(nibName: NSNib.Name(rawValue: "ViewController"), bundle: nil)

        rootController.contentViewController = controller

        logManager.enable()

        if logManager.logs.count == 0 {
            logManager.logStatus()
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    @objc
    func statusItemClick(_ sender: NSStatusBarButton) {

        rootController.show(relativeTo: sender.bounds, of: sender, preferredEdge: .minY)

        popoverMonitor = NSEvent.addGlobalMonitorForEvents(matching: [.leftMouseUp, .rightMouseUp], handler: { (event) in

            self.popoverMonitor = nil
            self.rootController.close()
        }) as? NSEvent
    }
}

