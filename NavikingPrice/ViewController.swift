//
//  ViewController.swift
//  NavikingPrice
//
//  Created by Wayne Lin on 2018/2/1.
//  Copyright © 2018年 Wayne Lin. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    @IBOutlet var arrayController: NSArrayController!
    
    @IBOutlet weak var priceLabel: NSTextField!

    @IBOutlet var controlMenu: NSMenu!

    override func viewDidLoad() {
        super.viewDidLoad()
        arrayController.content = (NSApp.delegate as! AppDelegate).logManager.logs
    }

    override func viewWillAppear() {
        super.viewWillAppear()
        arrayController.rearrangeObjects()

        if let arr = arrayController.arrangedObjects as? NSArray, let obj = arr.lastObject as? PriceTag {
            priceLabel.stringValue = obj.price as String
        } else {
            priceLabel.stringValue = "--"
        }
    }

    @IBAction func controlButtonAction(_ sender: NSButton) {

        guard let event = NSApp.currentEvent else {
            return
        }

        NSMenu.popUpContextMenu(controlMenu, with: event, for: sender)
    }
}
