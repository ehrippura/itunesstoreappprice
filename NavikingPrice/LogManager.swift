//
//  LogManager.swift
//  NavikingPrice
//
//  Created by Wayne Lin on 2018/2/1.
//  Copyright © 2018年 Wayne Lin. All rights reserved.
//

import Cocoa
import SwiftyJSON

class LogManager {

    private var timer: Timer?

    private let urlSession: URLSession = {
        let c = URLSessionConfiguration.default
        let s = URLSession(configuration: c)
        return s
    }()

    private(set) var logs = NSMutableArray()

    init() {
        let path = storePath()
        if !FileManager.default.fileExists(atPath: path) {
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        } else {
            if let l = NSKeyedUnarchiver.unarchiveObject(withFile: self.storePath() + "/logs") as? NSMutableArray {
                logs = l
            }
        }
    }

    func enable() {

        timer = Timer(fireAt: nextFireDate(), interval: 0, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: false)

        RunLoop.current.add(timer!, forMode: .defaultRunLoopMode)
    }

    func logStatus() {

        let req = URLRequest(url: URL(string: "https://itunes.apple.com/lookup?id=890791581&country=tw")!)

        urlSession.dataTask(with: req) { (data, res, err) in

            if let error = err {
                NSLog("%@", error.localizedDescription)
                return
            }

            let jsonObj = JSON(data!)

            let tag = PriceTag()
            tag.price = jsonObj["results"][0]["formattedPrice"].stringValue as NSString

            self.logs.add(tag)

            NSKeyedArchiver.archiveRootObject(self.logs, toFile: self.storePath() + "/logs")

            if UserDefaults.standard.bool(forKey: "FireNotification") {
                DispatchQueue.main.async {
                    let notification = NSUserNotification()
                    notification.title = "Naviking 3D \(tag.price): \(tag.date)"
                    NSUserNotificationCenter.default.scheduleNotification(notification)
                }
            }

        }.resume()
    }

    @objc private func timerFired(_ ti: Timer) {

        logStatus()

        timer = Timer(fireAt: nextFireDate(), interval: 0, target: self, selector: #selector(timerFired(_:)), userInfo: nil, repeats: false)

        RunLoop.current.add(timer!, forMode: .defaultRunLoopMode)
    }

    func nextFireDate() -> Date {

        let cal = Calendar.current
        var component = cal.dateComponents([.year, .month, .day, .hour, .minute], from: Date())

        if component.minute! >= 0 && component.minute! < 30 {
            component.minute = 30
        } else {
            component.hour! += 1
            component.minute = 0
        }

        return cal.date(from: component)!
    }

    private func storePath() -> String {
        let support = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory, .userDomainMask, true).first!
        return (support as NSString).appendingPathComponent(Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as! String) as String
    }
}
