//
//  PriceTag.swift
//  NavikingPrice
//
//  Created by Wayne Lin on 2018/2/1.
//  Copyright © 2018年 Wayne Lin. All rights reserved.
//

import Cocoa

class PriceTag: NSObject, NSSecureCoding {

    @objc var price: NSString

    @objc var date: NSDate

    class var supportsSecureCoding: Bool {
        return true
    }

    override init() {
        price = "0"
        date = NSDate()
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {

        price = aDecoder.decodeObject(of: NSString.self, forKey: "price")!
        date = aDecoder.decodeObject(of: NSDate.self, forKey: "date")!

        super.init()
    }

    func encode(with aCoder: NSCoder) {

        aCoder.encode(price, forKey: "price")
        aCoder.encode(date, forKey: "date")
    }
}
